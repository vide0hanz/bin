#!/bin/sh

id=$(xdo id -N music);
[ -z "$id" ] && st -c music -e ncmpcpp -s media_library

action='hide';
if [[ $(xprop -id $id | awk '/window state: / {print $3}') == 'Withdrawn' ]]; then
	action='show'
fi

xdo $action -N music
