#!/bin/bash

. "${HOME}/.cache/wal/colors.sh"

declare options=("STEREO
HEADSET")

choice=$(echo -e "${options[@]}" | dmenu -nb "$color0" -nf "$color7" -sb "$color0" -sf "$color15" -i -p 'Listen on:')

case "$choice" in
	STEREO)
		choice=$(asoundconf set-default-card HDAMP1) ;;
	HEADSET)
		choice=$(asoundconf set-default-card He) ;;
esac
