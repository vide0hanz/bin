#!/bin/bash
# Script intended to be executed via bspwmrc, sets shadows on floating windows and removes their borders #

bspc subscribe node_state | while read -r _ _ _ node state status; do
	if [[ "$state" == "floating" ]]; then
		case "$status" in
			off) xprop -id "$node" -remove _COMPTON_SHADOW &
				bspc config -n "$node" border_width 2 ;;
			on) xprop -id "$node" -f _COMPTON_SHADOW 32c -set _COMPTON_SHADOW 1 &
				bspc config -n "$node" border_width 0 ;;
		esac
	fi
done
